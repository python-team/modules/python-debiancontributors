# coding: utf8
# Debian Contributors data source data mining tools
#
# Copyright (C) 2014  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from .. import scanner

__all__ = ["SvnDirs"]

class SvnDirs(scanner.Scanner):
    """
    Scan subversion directories using file attributes to detect contributions.

    Generates `login` types of identifiers, using the usernames of the system
    where it is run.

    Example::

        contribution: committer
        method: svndirs
        dirs: /srv/svn.debian.org/svn/collab-maint
        url: https://alioth.debian.org/users/{user}/
    """
    dirs = scanner.GlobField(blank=False, help_text="""
                             subversion directories to scan. You can give one or more,
                             and even use shell-style globbing.
                             """)
    url = scanner.CharField(help_text="""
                            template used to build URLs to link to people's contributions.
                            ``{user}`` will be replaced with the username
                            """)

    def scan(self):
        from .utils.filesystem import Filesystem

        scan = Filesystem()
        for d in self.dirs:
            scan.scan_svn_repo(d)
        if self.url:
            tpl = self.url
            for ident, begin, end in scan.contributions():
                yield ident, begin, end, tpl.format(user=ident.id)
        else:
            for ident, begin, end in scan.contributions():
                yield ident, begin, end, None
