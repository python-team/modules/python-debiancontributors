#!/usr/bin/env/python
"""
Copyright (C) 2013--2014 Enrico Zini <enrico@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from setuptools import setup

setup(
    name="debiancontributors",
    requires=['requests (>=2.0.0)'],
    version="0.7.11",
    description="Manage submissions to contributors.debian.org",
    author="Enrico Zini",
    author_email="enrico@debian.org",
    url="https://salsa.debian.org/python-team/modules/python-debiancontributors",
    license="http://www.gnu.org/licenses/lgpl-3.0.html",
    packages=["debiancontributors", "debiancontributors.scanners", "debiancontributors.scanners.utils"],
    scripts=['dc-tool'],
)
