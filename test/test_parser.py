# coding: utf8
#
# Copyright (C) 2013  Enrico Zini <enrico@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import debiancontributors.parser as parser
import unittest

class TestParser(unittest.TestCase):
    def testGetKey(self):
        self.assertEqual(parser.get_key({"foo": "bar"}, "foo"), "bar")
        self.assertRaises(parser.Fail, parser.get_key, {}, "foo")

    def testGetKeyInt(self):
        self.assertEqual(parser.get_key_int({"foo": "7"}, "foo"), 7)
        self.assertRaises(parser.Fail, parser.get_key_int, {}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_int, {"foo": ""}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_int, {"foo": "seven"}, "foo")

    def testGetKeyString(self):
        self.assertEqual(parser.get_key_string({"foo": "7"}, "foo"), "7")
        self.assertEqual(parser.get_key_string({"foo": ""}, "foo", True), "")
        self.assertEqual(parser.get_key_string({"foo": None}, "foo", True), "")
        self.assertEqual(parser.get_key_string({}, "foo", True), "")
        self.assertRaises(parser.Fail, parser.get_key_string, {}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_string, {"foo": ""}, "foo")

    def testGetKeyUnicode(self):
        self.assertEqual(parser.get_key_unicode({"foo": "7"}, "foo"), "7")
        self.assertEqual(parser.get_key_unicode({"foo": b"\xe2\x99\xa5"}, "foo"), "♥")
        self.assertEqual(parser.get_key_unicode({"foo": ""}, "foo", True), "")
        self.assertEqual(parser.get_key_unicode({"foo": None}, "foo", True), "")
        self.assertEqual(parser.get_key_unicode({}, "foo", True), "")
        self.assertRaises(parser.Fail, parser.get_key_unicode, {}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_unicode, {"foo": ""}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_unicode, {"foo": b'\xe0'}, "foo")

    def testGetKeySequence(self):
        self.assertEqual(parser.get_key_sequence({"foo": []}, "foo"), [])
        self.assertEqual(parser.get_key_sequence({"foo": [1, 2, "three"]}, "foo"), [1, 2, "three"])
        self.assertEqual(parser.get_key_sequence({"foo": ()}, "foo"), ())
        self.assertRaises(parser.Fail, parser.get_key_sequence, {}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_sequence, {"foo": "bar"}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_sequence, {"foo": {}}, "foo")

    def testGetKeySequenceOrObject(self):
        self.assertEqual(parser.get_key_sequence_or_object({"foo": []}, "foo"), [])
        self.assertEqual(parser.get_key_sequence_or_object({"foo": {}}, "foo"), [{}])
        self.assertEqual(parser.get_key_sequence_or_object({"foo": [{}]}, "foo"), [{}])
        self.assertRaises(parser.Fail, parser.get_key_sequence_or_object, {}, "foo")
        self.assertRaises(parser.Fail, parser.get_key_sequence_or_object, {"foo": "bar"}, "foo")

    def testGetKeyDateOrNone(self):
        from datetime import date
        self.assertEqual(parser.get_key_date_or_none({"foo": "2013-11-16"}, "foo"), date(2013, 11, 16))
        self.assertEqual(parser.get_key_date_or_none({"foo": ""}, "foo"), None)
        self.assertEqual(parser.get_key_date_or_none({"foo": None}, "foo"), None)
        self.assertEqual(parser.get_key_date_or_none({}, "foo"), None)
        self.assertRaises(parser.Fail, parser.get_key_date_or_none, {"foo": "2013"}, "foo")

if __name__ == '__main__':
    unittest.main()
