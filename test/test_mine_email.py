# coding: utf8
#
# Copyright (C) 2018 Daniele Tricoli <eriol@mornie.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import unittest

from debiancontributors.datamine import DataMine


class TestMineEmail(unittest.TestCase):

    def test_email_scanner(self):
        """
        Test email scanner
        """
        mine = DataMine(configstr="""
source: test

contribution: developer
method: mailfrom
folders: test/email/*
url: http://www.example.com/{email}
""")
        mine.scan()

        emails = set(x.id for x in mine.submission.entries.keys())
        self.assertNotIn('eriol@mornie.org', emails)
        self.assertIn('jmw@debian.org', emails)


if __name__ == '__main__':
    unittest.main()
